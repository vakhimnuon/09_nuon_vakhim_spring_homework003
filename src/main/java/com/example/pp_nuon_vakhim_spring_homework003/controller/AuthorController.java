package com.example.pp_nuon_vakhim_spring_homework003.controller;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Authors;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.AuthorRequest;
import com.example.pp_nuon_vakhim_spring_homework003.model.response.ApiResponse;
import com.example.pp_nuon_vakhim_spring_homework003.service.AuthorService;
import lombok.Builder;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class AuthorController {
    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }
//   Get all author
    @GetMapping("/api/v1/authors/show")
    public ResponseEntity<?> getAllAuthor(){
//        return ResponseEntity.ok(new ApiResponse<List<Authors>>(
//                LocalDateTime.now(), HttpStatus.OK,
//                "Show  customer successfully",
//                authorService.getAllAuthor()
//        ));
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMessage("Successful");
        apiResponse.setStatus(HttpStatus.OK);
        apiResponse.setPayload(authorService.getAllAuthor());
        return ResponseEntity.ok().body(apiResponse);
    }
    @GetMapping("/api/v1/authors/read/{id}")
    public ResponseEntity<?> readAuthorById(@PathVariable Integer id){
        Authors authors = authorService.readAuthorById(id);
        ApiResponse<Authors> response = new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Read Author successfuully",
                authors);
          return ResponseEntity.ok(response);
    }
    @PostMapping("/api/v1/authors/insert")
    public ResponseEntity<?> insertAuthor(@RequestBody Authors authors){
        return ResponseEntity.ok(new ApiResponse<Authors>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert author Successfully",
                authorService.insertAuthor(authors)
        ));
    }
    @PutMapping("api/v1/authors/update/{id}")
    public ResponseEntity<?> updateAuthor(@PathVariable Integer id, @RequestBody AuthorRequest authorRequest){
        authorService.updateAuthorById(id , authorRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Update author Successfully",
                null
        ));
    }
    @DeleteMapping("api/v1/author/delete/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable Integer id){
        authorService.deleteAuthorById(id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Delete author Successfully",
                null
        ));
    }

}
