package com.example.pp_nuon_vakhim_spring_homework003.controller;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Categories;
import com.example.pp_nuon_vakhim_spring_homework003.model.response.ApiResponse;
import com.example.pp_nuon_vakhim_spring_homework003.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("api/v1/category/show")
    public ResponseEntity<?> getAllCateo(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMessage("Successful");
        apiResponse.setStatus(HttpStatus.OK);
        apiResponse.setPayload(categoryService.getAllCateo());
        return ResponseEntity.ok().body(apiResponse);
    }
    @GetMapping("api/v1/category/read/{id}")
    public ResponseEntity<?> readCateById(@PathVariable Integer id){
        Categories categories = categoryService.readCateById(id);
        ApiResponse<Categories> response = new ApiResponse<>(
                LocalDateTime.now(), HttpStatus.OK,
                "Read categories successfully",
                categories
        );
        return ResponseEntity.ok(response);
    }
}
