package com.example.pp_nuon_vakhim_spring_homework003.controller;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Books;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.BookRequest;
import com.example.pp_nuon_vakhim_spring_homework003.model.response.ApiResponse;
import com.example.pp_nuon_vakhim_spring_homework003.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
@RestController
public class BookController {
    private final BookService bookService;
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping("api/v1/books/show")
    public ResponseEntity<?> getAllBook(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMessage("Successful");
        apiResponse.setStatus(HttpStatus.OK);
        apiResponse.setPayload(bookService.getAllBook());
        return ResponseEntity.ok().body(apiResponse);
    }
    @GetMapping("api/v1/books/read/{id}")
    public ResponseEntity<?> readBookById(@PathVariable Integer id){
        Books books = bookService.readBookById(id);
        ApiResponse<Books> response = new ApiResponse<>(
                LocalDateTime.now(), HttpStatus.OK,
                "Read book successfully",
                books
        );
        return ResponseEntity.ok(response);
    }
    @PostMapping("api/v1/books/insert")
    public ResponseEntity<?> insertBook(@RequestBody Books books){
        bookService.insertBook(books);
        return ResponseEntity.ok(new ApiResponse<Books>(LocalDateTime.now(),
                HttpStatus.OK,
                "Update book Successfully",
                books
        ));
    }
    @PutMapping("api/v1/books/update/{id}")
    public ResponseEntity<?> updateBook(@PathVariable Integer id, @RequestBody BookRequest bookRequest){
        bookService.updateBookById(id, bookRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Update book Successfully",
                null
        ));
    }
    @DeleteMapping("api/v1/books/delete/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable Integer id){
        bookService.deleteById(id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Delete book Successfully",
                null
        ));
    }
}
