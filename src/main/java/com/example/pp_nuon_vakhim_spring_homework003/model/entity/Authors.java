package com.example.pp_nuon_vakhim_spring_homework003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Authors {
    private int authorId;
    private String authorName;
    private String gender;
}
