package com.example.pp_nuon_vakhim_spring_homework003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Books {
    private Integer bookId;
    private String title;
    private String dateRelease;
    private Authors authors;
    private List<Categories> categories;
}
