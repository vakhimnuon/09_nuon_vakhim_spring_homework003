package com.example.pp_nuon_vakhim_spring_homework003.model.request;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Authors;

import java.util.List;

public class BookRequest {
    private String title;
    private String dateRelease;
    private Integer authorId;
    private List<Integer> categoryId;
}
