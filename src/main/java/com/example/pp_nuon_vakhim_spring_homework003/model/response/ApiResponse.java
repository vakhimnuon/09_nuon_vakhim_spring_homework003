package com.example.pp_nuon_vakhim_spring_homework003.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiResponse<T> {
    @Builder.Default
//    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private LocalDateTime timestamp = LocalDateTime.now();

//    String formattedTimestamp = timestamp.format(formatter);

    private HttpStatus status;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)

    private T payload;
}
