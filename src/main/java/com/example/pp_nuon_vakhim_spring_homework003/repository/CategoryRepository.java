package com.example.pp_nuon_vakhim_spring_homework003.repository;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Categories;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("""
            SELECT * FROM categories
            """)
    @Results(id = "categoryMap" , value = {
            @Result(property = "categoryId" , column = "category_id"),
            @Result(property = "categoryName", column = "category_name")
    })
    List<Categories> getCate();
    @Select("""
            SELECT c.category_id, c.category_name
                    FROM categories c INNER JOIN book_details bd on c.category_id = bd.category_id
            WHERE book_id=#{id};
            """)
    @Result(property = "categoryId" , column = "category_id")
    @Result(property = "categoryName", column = "category_name")
    Categories readCateById(@Param("id") Integer id);
}
