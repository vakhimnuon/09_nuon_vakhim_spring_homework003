package com.example.pp_nuon_vakhim_spring_homework003.repository;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Books;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("""
            SELECT * FROM books
            """)
    @Result(property = "bookId" , column = "book_id")
    @Result(property = "dateRelease", column = "published_date")
    @Result(property = "authors", column ="author_id",
            one = @One(select = "com.example.pp_nuon_vakhim_spring_homework003.repository.AuthorRepository.readAuthorById")
    )
    @Result(property = "categories", column = "book_id",
            many = @Many(select = "com.example.pp_nuon_vakhim_spring_homework003.repository.CategoryRepository.readCateById")
    )
    List<Books> getAllBook();
    @Select("""
            SELECT * FROM books
            WHERE book_id =#{id};
            """)
    @Result(property = "bookId" , column = "book_id")
    @Result(property = "dateRelease", column = "published_date")
    @Result(property = "authors", column ="author_id",
            one = @One(select = "com.example.pp_nuon_vakhim_spring_homework003.repository.AuthorRepository.readAuthorById")
    )
    @Result(property = "categories", column = "book_id",
    many = @Many(select = "com.example.pp_nuon_vakhim_spring_homework003.repository.CategoryRepository.readCateById")
    )
    Books readBookById(Integer id);
    @Select("""
            INSERT INTO books (title,published_date, author_id)
            VALUES (#{book.title}, #{book.dateRelease},#{book.authorId})
            """)
    Books insertBook(@Param("book") Books bookRequest);
}
