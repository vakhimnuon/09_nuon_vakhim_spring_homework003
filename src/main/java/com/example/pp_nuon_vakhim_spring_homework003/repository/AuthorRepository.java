package com.example.pp_nuon_vakhim_spring_homework003.repository;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Authors;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.AuthorRequest;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {

    @Select("""
             SELECT * FROM authors
             """)
    @Results(id = "authorMap" , value = {
            @Result(property = "authorId" , column = "author_id"),
            @Result(property = "authorName", column = "author_name")
    })
    List<Authors> getAllAuthor();
    @Select("""
            SELECT * FROM authors
            WHERE author_id =#{id};
            """)
    @ResultMap("authorMap")
    Authors readAuthorById(Integer id);
    @Select("""
            INSERT INTO authors (author_name, gender)
            values (#{authors.authorName},#{authors.gender})
            RETURNING *           
            """)
    @ResultMap("authorMap")
    Authors insertAuthor(@Param("authors") Authors authors);
    @Update("""
            UPDATE authors
            SET author_name = #{author.name}, gender=#{author.gender}
            WHERE author_id =#{author_id};
            """)
    void updateAuthorById(@Param("author_id") Integer id,@Param("author") AuthorRequest authorRequest);
    @Delete("""
           DELETE FROM authors
           WHERE author_id=#{id};
            """)
    void deleteAuthorById(Integer id);
}
