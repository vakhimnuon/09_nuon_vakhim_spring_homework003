package com.example.pp_nuon_vakhim_spring_homework003.service;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Authors;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.AuthorRequest;

import java.util.List;
public interface AuthorService {

   List<Authors>  getAllAuthor();

    Authors readAuthorById(Integer id);

    Authors insertAuthor(Authors authors);

    void updateAuthorById(Integer id, AuthorRequest authorRequest);

    void deleteAuthorById(Integer id);


    //Authors updateAuthorById(Integer id, AuthorService authorRequest);
}
