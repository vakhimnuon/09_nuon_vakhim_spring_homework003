package com.example.pp_nuon_vakhim_spring_homework003.service.serviceImpl;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Authors;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.AuthorRequest;
import com.example.pp_nuon_vakhim_spring_homework003.repository.AuthorRepository;
import com.example.pp_nuon_vakhim_spring_homework003.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class AuthorServiceImpl implements AuthorService {
    private AuthorRepository authorRepository;
@Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Authors> getAllAuthor() {
        return authorRepository.getAllAuthor();
    }

    @Override
    public Authors readAuthorById(Integer id) {
        return authorRepository.readAuthorById(id);
    }

    @Override
    public Authors insertAuthor(Authors authors) {
        return authorRepository.insertAuthor(authors);
    }

    @Override
    public void updateAuthorById(Integer id, AuthorRequest authorRequest) {
        authorRepository.updateAuthorById(id, authorRequest);
    }

    @Override
    public void deleteAuthorById(Integer id) {
        authorRepository.deleteAuthorById(id);
    }

//    @Override
//    public Authors updateAuthorById(Integer id, AuthorService authorRequest) {
//        authorRepository.updateAuthorById(id, (AuthorRequest) authorRequest);
//        return authorRepository.readAuthorById(id);
//    }





}

