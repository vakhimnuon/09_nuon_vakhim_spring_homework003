package com.example.pp_nuon_vakhim_spring_homework003.service.serviceImpl;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Books;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.BookRequest;
import com.example.pp_nuon_vakhim_spring_homework003.repository.BookRepository;
import com.example.pp_nuon_vakhim_spring_homework003.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private  BookRepository bookRepository;
@Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Books> getAllBook() {
        return bookRepository.getAllBook();
    }

    @Override
    public Books readBookById(Integer id) {
        return bookRepository.readBookById(id);
    }


    @Override
    public void insertBook(Books books) {
        bookRepository.insertBook(books);
    }

    @Override
    public void updateBookById(Integer id, BookRequest bookRequest) {

    }

    @Override
    public void deleteById(Integer id) {

    }
}
