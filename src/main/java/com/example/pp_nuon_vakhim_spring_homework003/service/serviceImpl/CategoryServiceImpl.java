package com.example.pp_nuon_vakhim_spring_homework003.service.serviceImpl;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Categories;
import com.example.pp_nuon_vakhim_spring_homework003.repository.CategoryRepository;
import com.example.pp_nuon_vakhim_spring_homework003.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;
    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Categories> getAllCateo() {
        return categoryRepository.getCate();
    }

    @Override
    public Categories readCateById(Integer id) {
        return categoryRepository.readCateById(id);
    }
}
