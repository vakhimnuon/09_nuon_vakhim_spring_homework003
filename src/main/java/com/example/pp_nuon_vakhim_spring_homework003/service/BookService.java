package com.example.pp_nuon_vakhim_spring_homework003.service;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Books;
import com.example.pp_nuon_vakhim_spring_homework003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Books> getAllBook();

    Books readBookById(Integer id);


    void insertBook(Books bookRequest);

    void updateBookById(Integer id, BookRequest bookRequest);

    void deleteById(Integer id);
}
