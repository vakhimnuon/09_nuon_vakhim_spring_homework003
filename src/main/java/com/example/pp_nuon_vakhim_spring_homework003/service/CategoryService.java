package com.example.pp_nuon_vakhim_spring_homework003.service;

import com.example.pp_nuon_vakhim_spring_homework003.model.entity.Categories;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {
    List<Categories> getAllCateo();

    Categories readCateById(Integer id);
}
