CREATE DATABASE homework003;

CREATE TABLE authors (
                         author_id serial primary key not null,
                         author_name varchar(50) not null ,
                         gender varchar(50) not null
);

CREATE TABLE books (
                       book_id serial primary key not null,
                       title varchar(255) not null ,
                       published_date timestamp,
                       author_id INT NOT NULL ,
                       FOREIGN KEY (author_id) REFERENCES authors (author_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE book_details(
                    id serial primary key not null ,
                    book_id INT not null ,
                    category_id INT not null ,
                    FOREIGN KEY (book_id) REFERENCES authors (author_id) ON DELETE CASCADE ON UPDATE CASCADE,
                    FOREIGN KEY (category_id) REFERENCES categories (category_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE categories (
                    category_id serial primary key not null ,
                    category_name varchar(255) not null
);
